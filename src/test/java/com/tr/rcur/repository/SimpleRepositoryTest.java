package com.tr.rcur.repository;

import com.tr.rcur.domain.SimpleDomain;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SimpleRepositoryTest {
    @Autowired
    SimpleRepository simpleRepository;

    @Test
    public void findAfterSave() throws Exception {
        simpleRepository.save(new SimpleDomain());

        Assertions.assertThat(simpleRepository.findById(1L)).isNotNull();
        Assertions.assertThat(simpleRepository.findAll().size()).isEqualTo(1);

    }
}
