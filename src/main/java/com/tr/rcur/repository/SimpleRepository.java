package com.tr.rcur.repository;


import com.tr.rcur.domain.SimpleDomain;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface SimpleRepository extends Repository<SimpleDomain, Long> {
    SimpleDomain save(SimpleDomain simpleDomain);
    SimpleDomain findById(Long id);
    List<SimpleDomain> findAll();
}
