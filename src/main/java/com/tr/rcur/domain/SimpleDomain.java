package com.tr.rcur.domain;

import javax.persistence.*;

@Entity
@Table(name = "Simple_Domain")
public class SimpleDomain {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "SIMPLEDOMAIN_ID_SEQ")
    @SequenceGenerator(name = "SIMPLEDOMAIN_ID_SEQ",
            sequenceName = "SIMPLEDOMAIN_ID_SEQ", allocationSize = 1)
    @Column(updatable = false)

    private Long permID;
    private String organizationName;
    private Boolean isPublic;
    private Status status;
    private String website;
    private String ultimateParent;
    private String intemidiateParent;
    private String hQAddress;
    private String registeredAddress;
    private String lEI;
    private String aVID;
    private String primaryIndustry;
    private String primaryBusinessSector;
    private String primaryEconomicSector;
    private String domiciledIn;
}
