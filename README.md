# Sample RCUR service skeleton

## Prerequisites

* [Java JDK 1.8.172](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Gradle 4.7](https://gradle.org/install/#manually)
* Eclipse or IDEA
    * If using Eclipse, then [STS](https://spring.io/tools/sts/all) is the preferred Eclipse flavor, you may need to install the [Buildship Gradle Integration plugin](https://marketplace.eclipse.org/content/buildship-gradle-integration) for Gradle support 

### Eclipse way of pain

* [Eclipse oxygen](https://www.eclipse.org/downloads/download.php?file=/oomph/epp/oxygen/R2/eclipse-inst-win64.exe)
* [Lombok plugin](https://projectlombok.org/setup/eclipse)

## Dependencies

All dependencies will be downloaded automatically

* Spring Boot 2.0.1.RELEASE
* Spring Framework 5.0.5.RELEASE

## Running the application

1. (Optional) Put proper path to file storage into *src\main\resources\application.properties*
2. Run with **gradlew bootRun** (or **gradle bootRun** if your have gradle installed to your system) 
     * This task will execute **npm install** and **npm run build** for the web project and copy build artefacts
3. Open in browser 
     * [Home page](http://localhost:8090/)
     * [Groups](http://localhost:8090/api/groups/)

# Documentation

## References 

* [Uploading Files](https://spring.io/guides/gs/uploading-files/)
* [File Upload with Spring MVC](http://www.baeldung.com/spring-file-upload)
* [Spring Boot with Docker](https://spring.io/guides/gs/spring-boot-docker/)
* [Gradle docker plugin](https://plugins.gradle.org/plugin/com.palantir.docker)
* [Spring Data Annotations](http://www.baeldung.com/spring-data-annotations)

## Security

* [Spring Security Architecture](https://spring.io/guides/topicals/spring-security-architecture/)
* [Spring Security Custom Authentication Provider](http://www.baeldung.com/spring-security-authentication-provider)
* [Spring Security 5 - Custom UserDetailsService example](https://www.boraji.com/spring-security-5-custom-userdetailsservice-example)

## AWS Integration

* [Install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-windows.html#awscli-install-windows-path)
* [Python 3.6.2](https://www.python.org/downloads/release/python-362/)
     * pip install awscli
* [Configure AWS credentials](https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html)
     * easiest way is to create C:\Users\USERNAME\\.aws\credentials file and put following information:

```
[default]
aws_access_key_id = your_access_key_id
aws_secret_access_key = your_secret_access_key
```
 
## SAFE

* [How to Integrate Safe CAAST in Aws Server-less](https://thehub.thomsonreuters.com/people/8015921/blog/2017/09/15/how-to-integrate-safe-caast-in-aws-server-less-apps)
* [Hacking Safe Authentication Process](https://thehub.thomsonreuters.com/docs/DOC-2447806)